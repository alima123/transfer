module bitbucket.org/alima123/transfer

go 1.13

require (
	bitbucket.org/alima123/util v0.0.0-20191226180549-8e7ab4020abd
	github.com/golang/protobuf v1.3.2
)
